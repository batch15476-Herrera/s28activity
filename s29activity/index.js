const express = require("express");  

const port = 4000; 

const application = express();

application.use(express.json());  

application.listen(port, () => console.log(`Express API Server on port: ${port}`)); 

//"GET METHOD add task"
application.get('/', (request, response) => {
  response.send('Welcome to my to do list server!'); 
});

const tasks = []; 

 
  application.get('/tasks', (req, res) => {
        res.send(tasks); 
  });


  //"POST METHOD add task"
  application.post('/task', (req, res) => {
     let newTask = req.body;
     let taskName = req.body.name; 
     let taskStat = req.body.status;
     
     if (taskName !== '' && taskStat !== '' && taskName.length !==0) {
       
       tasks.push(newTask); 
       res.send(`New ${taskName} Task has been added in our To-do list`);
                 
     } else {
        
        res.send('Make sure that Task Name and Status field is complete');
     }
  });

//"PUT METHOD change status"
  application.put('/update-status', (req, res) => {
     
      let message; 
      
      let cTarget = req.body.name;
      let cUpdate = req.body.status;
      if (tasks.length !== 0) {
        
        for (let index = 0;index < tasks.length; index++) {
           
           if (cTarget === tasks[index].name) {
               
              tasks[index].status = cUpdate; 

              message = `${cTarget} Found and Updated to ${cUpdate}`;
              
              break; 
           } else {
              
              message = `No Match Found for ${cTarget}`;
           };
        };
      } else {
        
        message = 'NO AVAILABLE TASK FOUND';
      };
      res.send(message);
  });


  //"PUT METHOD change name"

  application.put('/update-name', (req, res) => {
      
      let uTarget = req.body.status;
      let uUpdate = req.body.name; 
      let message;       
      
      
      if (tasks.length !== 0) {
        
        for (let indexNum = 0; indexNum < tasks.length; indexNum++) {
           
           let taskStatus = tasks[indexNum].status;
           let currentName = tasks[indexNum].name; 
           if (uTarget === taskStatus) {

              message = `Match Found for ${uTarget} and updated status from ${currentName} to ${uUpdate}`;
              tasks[indexNum].name = uUpdate; 
              
              break; 
           } else {
              
              message = `No Match Found for ${uTarget}`;
           }; 
        };
      } else {
        
        message = 'users not found';
      }

      res.send(message);
  });


//"DEL METHOD"
  application.delete('/task', (req, res) => {
       
      let message;
      
      let targetReference = req.body.name;

      if (tasks.length !== 0) {
         
         for (let index = 0; index < tasks.length; index++) {
            
            if (targetReference === tasks[index].name) {
               
              tasks.splice(index, 1);
              message = `A Match for ${targetReference} has been found and Deleted from the resources`;
              
              break; 
            } else {
              message = 'Wala ko nakita!';
            };  
         };
      } else {
         message = 'EMPTY collection';
      };

      
      res.send(message);
  });